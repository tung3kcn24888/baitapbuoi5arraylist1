import java.util.*;

public class BaiTapBuoi5ArrayList {
    public static void main(String[] args) {
        Bai1();
        Bai2();
        Bai3();
        Bai6();
        bai7();
        Bai9();
        Bai10();
        Bai11();
        Bai12();
        Bai20();
        Bai21();
        Bai1ArrayList();
    }

    private static void Bai1() {
        //Them 6 sv vao ArrayList Sau do Sap xep them ten
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(01, "Tung", "java"));
        students.add(new Student(02, "Thai", "c#"));
        students.add(new Student(03, "Hieu", "java"));
        students.add(new Student(04, "Anh", "java"));
        students.add(new Student(05, "Bao", "javaScrip"));
        students.add(new Student(06, "Trung", ".Net"));
        students.sort(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        for (Student s : students) {
            System.out.print(s.getName() + " ");
        }
    }

    private static void Bai2() {
        //Tong Cac Phan tu trong arraylist
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(01, "Tung", "java"));
        students.add(new Student(02, "Thai", "c#"));
        students.add(new Student(03, "Hieu", "java"));
        students.add(new Student(04, "Anh", "java"));
        students.add(new Student(05, "Bao", "javaScrip"));
        students.add(new Student(06, "Trung", ".Net"));
        int sum = 0;
        for (Student s : students) {
            sum += s.getId();
        }
        System.out.println("\n" + sum);
    }

    public static void Bai3() {
        //Them phan tu vao Array neu khong trung id
        LinkedList<Student> students = new LinkedList<>();
        students.add(new Student(01, "Tung", "java"));
        students.add(new Student(02, "Thai", "c#"));
        students.add(new Student(03, "Hieu", "java"));
        students.add(new Student(04, "Anh", "java"));
        students.add(new Student(05, "bao", "java"));
        students.add(new Student(06, "Trung", "java"));
        Student svAdd = new Student(07, "Hai", "font end");
        int dem = 0;
        for (Student s : students) {
            if (svAdd.getId() == s.getId()) {
                dem++;
            }
        }
        if (dem == 0) {
            students.add(svAdd);
            System.out.println("add Thanh Cong");
        }
        for (Student s : students) {
            System.out.print(s.getName() + " ");
        }
    }

    public static void Bai6() {
        //Tim chi so cua 1 phan tu
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(01, "Tung", "java"));
        students.add(new Student(02, "Thai", "c#"));
        students.add(new Student(03, "Hieu", "java"));
        students.add(new Student(04, "Anh", "java"));
        students.add(new Student(05, "Bao", "javaScrip"));
        students.add(new Student(06, "Trung", ".Net"));
        Student sv = new Student(01, "Tung", "java");
        students.add(sv);
//        System.out.println(students.get(0).getName());
        System.out.println(students.indexOf(sv));
    }

    private static void bai7() {
        //Xoa phan tu cu the tu arrayList
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(01, "Tung", "java"));
        students.add(new Student(02, "Thai", "c#"));
        students.add(new Student(03, "Hieu", "java"));
        students.add(new Student(04, "Anh", "java"));
        students.add(new Student(05, "Bao", "javaScrip"));
        students.add(new Student(06, "Trung", ".Net"));
        Student sv = new Student(01, "Tung", "java");
        for (int i = 0; i < students.size(); i++) {
            if ((students.get(i).getId() == sv.getId()) && (students.get(i).getName().compareToIgnoreCase(sv.getName()) == 0) && (students.get(i).getMajor().compareToIgnoreCase(sv.getMajor()) == 0)) {
                students.remove(students.get(i));
            }
        }
        for (Student s : students) {
            System.out.print(s.getName() + " ");
            ;
        }
    }

    private static void Bai9() {
        //them 1 student vao vi tri cho truoc
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(01, "Tung", "java"));
        students.add(new Student(02, "Thai", "c#"));
        students.add(new Student(03, "Hieu", "java"));
        students.add(new Student(04, "Anh", "java"));
        students.add(new Student(05, "Bao", "javaScrip"));
        students.add(new Student(06, "Trung", ".Net"));
        Student sv = new Student(01, "Tung", "java");
        students.add(2, sv);
        System.out.println();
        for (Student s : students) {
            System.out.print(s.getName() + " ");
            ;
        }
    }

    private static void Bai10() {
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(01, "Tung", "java"));
        students.add(new Student(02, "Thai", "c#"));
        students.add(new Student(03, "Hieu", "java"));
        students.add(new Student(04, "Anh", "java"));
        students.add(new Student(05, "Bao", "javaScrip"));
        students.add(new Student(06, "Trung", ".Net"));
//        Student sv = new Student(01,"Tung","java");
        int max = students.get(0).getId();
        int min = students.get(0).getId();
        for (Student s : students) {
            if (s.getId() >= max) {
                max = s.getId();
            }
            if (s.getId() <= min) {
                min = s.getId();
            }
        }

        System.out.println("\nmax: " + max);
        System.out.println("min: " + min);
    }

    private static void Bai11() {
        //Dao nguoc cac phan tu trong AraayList
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(01, "Tung", "java"));
        students.add(new Student(02, "Thai", "c#"));
        students.add(new Student(03, "Hieu", "java"));
        students.add(new Student(04, "Anh", "java"));
        students.add(new Student(05, "Bao", "javaScrip"));
        students.add(new Student(06, "Trung", ".Net"));
        ArrayList<Student> students1 = new ArrayList<>();
        for (int i = students.size() - 1; i >= 0; i--) {
            students1.add(students.get(i));
        }
        for (Student s : students1) {
            System.out.print(s.getName() + " ");
            ;
        }
    }

    private static void Bai12() {
        // Tim phan tu xuat hien nhieu lan
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(01, "Tung", "java"));
        students.add(new Student(02, "Thai", "c#"));
        students.add(new Student(03, "Hieu", "java"));
        students.add(new Student(04, "Anh", "java"));
        students.add(new Student(01, "Trung", "javaScrip"));
        students.add(new Student(01, "Tung", "java"));
//        ArrayList<Student> students1 = new ArrayList<>();
        int b[] =new int[10];
        for (int i=0;i<students.size();i++){
            for (int j=i;j<students.size();j++){
                if (students.get(j).getId()==students.get(i).getId()){
                    b[i]++;
                }
            }
        }
        System.out.println();
        for (int i=0;i< b.length;i++){
            if (b[i]==2){
                System.out.println(students.get(i).getName() );
            }
        }
    }
    private static void Bai20() {
        //Chuyển Array sang ArrayList
        int a[]={2,3,4,6,7};
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        for (int i =0 ;i<a.length;i++){
            arrayList.add(a[i]);
        }
        System.out.println("Size of ArrayList: "+arrayList.size());
    }
    private static void Bai21() {
        //Chuyen doi tu ArrayList sang list
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        int a[] = new int[10];
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        for (int i =0;i<arrayList.size();i++){
            a[i]=arrayList.get(i);
            System.out.print(a[i]+" ");
        }

    }

    private static void Bai1ArrayList() {
        //Khoi tao ArrayList<String> va in ra ArrayList do
        ArrayList<String> strings = new ArrayList<>();
        strings.add("Sunday");
        strings.add("Monday");
        strings.add("Tueday");
        strings.add("Wednesday");
        strings.add("Thursday");
        strings.add("Friday");
        strings.add("Saturday");
        System.out.println();
        for (String s:
             strings) {
            System.out.print(s+" ");
        }
    }

}

